<?php
// Template Name: Informacije
?>
<?php 
get_header();
?>

<style>
    .tab-label{
      background: <?php echo $primary_color; ?>;
    }
    .tabs :first-child :checked ~ .tab-label {
      background: <?php echo $primary_color; ?>;  
    }
    .tab-label:hover {
        background: #1a252f;
    }
    .tabs:nth-child(2){
        background: #1a212f;
    }
    .main-header h1::before{      
        background: <?php echo $link_color ?>;
    }


</style>


<div class="container-fluid">
    <div class="container home-hero-cont">
    <div class="container">
        <div class="row information-row-wrapper">
        <div class="col-md-12 main-header">
            <h1 style = "color: <?php echo $first ?>">Informacije</h1>
        </div>
        <!-- Accordion START -->
        <div class="col-12">
            <div class="row">

            <div class="tabs">
            <?php
            if( have_rows('nova_informacija') ){
                $date_set = 1;
                // Loop through rows.
                while( have_rows('nova_informacija') ) {
                the_row();
                    // Load date
                $date_value_name = get_sub_field('naziv');
                $date_value_desc = get_sub_field('opis');
            ?>
                <div class="tab">
                    <input type="checkbox" id="<?php echo $date_set ?>">
                    <label class="tab-label" for="<?php echo $date_set ?>"><?php echo $date_value_name ?></label>
                    <div class="tab-content">
                    <?php echo $date_value_desc ?>
                    </div>
                </div>
            <?php
                $date_set += 1;
            }

            }
            ?>
            </div>
            </div>
            </div>
    </div>
</div>
</div>
</div>




<?php 
get_footer();
?>
