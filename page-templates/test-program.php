<?php
// Template Name: Test predavaci 
get_header();
?>

<div style="margin: 200px 0px;">

<?php 

$first_tabs = get_terms( array(
    'taxonomy' => 'vreme_predavanja',
    'hide_empty' => true,
    'parent'     => 0
) );


function nameStamp($i) {
    $dtime = DateTime::createFromFormat("d.m.Y", $i->name);
    return $dtime->getTimestamp();
}
usort($first_tabs, function ($a, $b) {
    return nameStamp($a) - nameStamp($b);
});


$full_program = array();

foreach($first_tabs as $date_tab) {
    $child_terms = get_terms( 'vreme_predavanja', array(
        'hide_empty' => true,
        'parent' => $date_tab->term_id,
    ));
    $dtime = DateTime::createFromFormat("d.m.Y", $date_tab->name);
    $timestamp = $dtime->getTimestamp();

    $mbrs = array();
    foreach($child_terms as $single_term) {
        $mbr = array();
        $members = get_posts(array(
            'post_type' => 'ucesnik',
            'posts_per_page' => -1,
            'meta_key' => 'pocetak_predavanja',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'vreme_predavanja',
                    'field' => 'term_id',
                    'terms' => $single_term->term_id,
                ),
                array(
                    'taxonomy' => 'kategorije_ucesnika',
                    'field' => 'slug',
                    'terms' => 'speaker'
                )
            )
        ));
        foreach($members as $member) {
            $mbr[] = array(
                'member_name' => $member->post_title,
                'member_theme' => get_field('naziv_predavanja', $member->ID),            
                'member_preofession' => get_field('titula_', $member->ID),
                'start_time' => get_field('pocetak_predavanja', $member->ID),
                'end_time' => get_field('kraj_predavanja', $member->ID),
                'member_image' => get_the_post_thumbnail_url($member->ID, 'member-thumbnail')
            );
        }  
        array_push($mbrs, array(
            'hall_name' => $single_term->name,
            'members' =>  $mbr
        ));
    }
    array_push($full_program, array(
        'date' => $timestamp,
        'members' =>  $mbrs
    ));
    
}

echo '<pre>';
var_dump($full_program);
echo '</pre>';


// $args = array(
//     'post_type' => 'ucesnik'
// );


// $ucesnik_posts = get_posts($args);

// $days = array();


// foreach($ucesnik_posts as $ucesnik) {
//     $ucesnik_day = get_field('datum_predavanja', $ucesnik->ID);
//     $unixtimestamp = strtotime( $ucesnik_day );
//     if (!in_array($unixtimestamp, $days)) {
//         $days[] = $unixtimestamp;
//     }
// }

// function sortByOrder($a, $b) {
//     return $a - $b;
//   }
// usort($days, 'sortByOrder');

// $full = array();
// $sale = array();


// foreach($days as $dy) {
//     $sales = array();
//     foreach($ucesnik_posts as $ups) { 
        
//         $ucesnik_d = get_field('datum_predavanja', $ups->ID);
//         $unixtimestamp = strtotime( $ucesnik_d );
//         if($dy ==  $unixtimestamp) {
//             $ucesnik_sala = get_field('naziv_sale', $ups->ID);
//             if (!in_array($ucesnik_sala, $sales)) {
//                 $sales[] = $ucesnik_sala;
//             }
            
//         }
//     }
//     array_push($sale, array(
//         'day' => $dy,
//         'sale' => $sales
        
//     ));
// }


// foreach($sale as $sl) {
//     $asd = array();
//     foreach($sl['sale'] as $sala_day) {
//         $boys = array();
//         foreach($ucesnik_posts as $uucs) { 
//             $ucesnik_dy = get_field('datum_predavanja', $uucs->ID);
//             $ucesnik_salas = get_field('naziv_sale', $uucs->ID);
//             $unixtimestamp = strtotime( $ucesnik_dy );
        
//             if($ucesnik_dy == $sl['day'] && $ucesnik_salas == $sala_day) {
//                 $boys[] = $uucs->post_title;
//             }
//         }
//         array_push($asd, array(
//             'sala' => $sala_day,
//             'ljudi' => $boys
            
//         ));

//     }
  
//     array_push($full, array(
//         'day' => $sl['day'],
//         'all' => $asd
        
//     ));

// }

// echo '<pre>';
// var_dump($full);
// echo '</pre>';


?>

</div>
<?php 
get_footer();
?>