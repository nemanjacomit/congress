<?php
// Template Name: Home
?>

<?php 
get_header();
?>

<!-- HERO SECTION START -->
<div class="container-fluid home-hero-fluid" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>);">
    <div class="row">
        <div class="container home-hero-cont">
            <div class="row">
                <div class="col-md-7 left-hero-col">
                    <div class="left-hero-section-wrapper">
                        <div class="title-box">
                            <?php 
                                $home_title = get_field('naslov_kongresa');
                                echo $home_title ? $home_title : ''; 
                            ?>
                        </div>
                        <div class="date-location-wrapper">
                            <div class="dates-wrapper">
                                <img src="<?php echo get_template_directory_uri() . '/inc/images/kalendar.png'; ?>">
                                <div class="home-dts ">
                                    <?php 
                                        $date_start = get_field('pocetak_kongresa');
                                        $date = DateTime::createFromFormat('Ymd', $date_start);
                                    ?>
                                    <span><?php echo $date->format('d'); ?></span>
                                    <span> - </span>
                                    <span><?php the_field('kraj_kongresa'); ?></span>
                                </div>
                            </div>
                            <div class="location-wrapper ">
                                <img src="<?php echo get_template_directory_uri() . '/inc/images/location.png'; ?>">
                                <span><?php the_field('lokacija'); ?></span>
                            </div>
                        </div>
                        <div class="date-counter-wrapper" id="time-counter">
                            <div class="date-titles">
                                <div><?php _e('DAYS', 'congress'); ?></div>
                                <div><?php _e('HOURS', 'congress'); ?></div>
                                <div><?php _e('MINUTS', 'congress'); ?></div>
                                <div><?php _e('SECONDS', 'congress'); ?></div>
                            </div>
                            <div id="date-data-fields"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 right-hero-col">
                    <div class="right-hero-boxes-wrapper">
                        <div class="top-hero-box-wrapper">
                            <div class="tb-hero-box">
                                <span class="tb-over"><?php _e('Over', 'congress'); ?></span>
                                <p><?php the_field('broj_predavaca'); ?></p>
                                <span class="tb-speakers"><?php _e('speakers', 'congress'); ?></span>
                            </div>
                        </div>
                        <div class="middle-hero-box">
                            <div class="mb-hero-box">
                                <span class="halls"><?php _e('halls', 'congress') ?></span>
                                <p><?php the_field('broj_sala_h'); ?></p>
                            </div>
                        </div>
                        <div class="bottom-hero-box">
                            <div class="bb-hero-box">
                                <h3> <?php _e('Topics', 'congress'); ?></h3>
                                    <?php the_field('teme'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- HERO SECTION END -->

<!-- ANNOUNCEMENT SECTION START -->
<div class="container-fluid annc-main-fluid" style="background-image: linear-gradient(185deg, #E3EDF2 70%, #FFF calc(70% + 2px));">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="col-12 annc-col" style="background: linear-gradient(180deg, rgba(255, 255, 255, 0.8) 36.03%, rgba(73, 186, 228, 0.8) 230.55%);">
                        <div class="row">
                            <div class="col-12 annc-title-col">
                                <h3 class="primary-color">
                                    <?php _e('DEAR COLLEAGUES AND FRIENDS', 'congress'); ?>
                                </h3>
                            </div>
                            <div class="col-md-3 annc-img-name-col">
                                <div class="annc-img-wrapper">
                                    <img src="<?php the_field('slika_predavaca'); ?>">
                                </div>
                                <div class="annc-name-wrapper">
                                    <p class="primary-color"><?php the_field('opis_predavaca'); ?></p>
                                </div>
                            </div>
                            <div class="col-md-9 annc-text-col">
                                <div class="annc-text-wrapper text-color">
                                    <?php the_field('tekst_predavaca'); ?>
                                </div>
                                <div class="annc-cta">
                                    <a class="link-background-color" href="<?php the_field('link_pd'); ?>"><?php _e('Read more', 'congress'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ANNOUNCEMENT SECTION END -->

<!-- NEWS SECTION START -->

<div class="container-fluid home-news-fluid">
    <div class="row">
        <div class="container home-news-wide-cont">
            <div class="row">
                
                <!-- BOTTOM SLIDER -->
                <?php 
                    $multi_slider = array(
                        'category_name' => 'vesti',
                        'posts_per_page' => 15,
                        'orderby'   => array(
                            'menu_order'=>'DESC',
                            'date' =>'DESC'
                            )        
                        );
                    $multi_posts = get_posts($multi_slider);
                ?>
                <div class="col-12">
                    <div class="home-multi-news-slider section-background-color">
                        <div class="multi-slider-white-wrapper">
                            <div class="swiper-container multi-slide-container" >
                                <div class="news-ttl">
                                    <h3 class="primary-color"><?php _e('News', 'congress'); ?></h3>
                                </div>
                                <div class="swiper-wrapper">
                                    <?php foreach($multi_posts as $mps) { ?>
                                        <div class="swiper-slide swiper-slide-next" style="width: 294.4px; margin-right: 30px;">
                                            <div class="slider-post-wrapper">
                                                <div class="slider-post-image">
                                                    <div class="ms-image-wrapper">
                                                        <img src="<?php echo has_post_thumbnail($mps) ? get_the_post_thumbnail_url($mps, 'custom-thumbnail') : get_template_directory_uri(). '/inc/images/default-thumbnail.png' ?>" loading="lazy">
                                                    </div>                                            
                                                    <div>
                                                        <div class="slider-post-date">
                                                            <?php echo get_the_date('d.m.Y', $mps); ?>                                               
                                                        </div>
                                                        <div class="slider-post-title">
                                                            <h3><?php echo $mps->post_title; ?></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-box-contnet-wrapper">
                                                    <div class="slider-post-button">
                                                        <a class="link-background-color" href="<?php echo get_permalink($mps); ?>"><?php _e('Read more', 'congress'); ?></a>
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="swiper-button-prev-unique ar-wrap" style="background-color: <?php echo $gradient_color_1 ?>80;">
                                    <div class="arrow-slider-wrapper">
                                        <img src="<?php echo get_template_directory_uri()?>/inc/images/prev-unique.png">
                                    </div>
                                </div>
                                <div class="swiper-button-next-unique ar-wrap" style="background-color: <?php echo $gradient_color_1 ?>80;">
                                    <div class="arrow-slider-wrapper">
                                        <img src="<?php echo get_template_directory_uri()?>/inc/images/next-unique.png">
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- NEWS SECTION END-->

<!-- PROGRAM SECTION -->
<div class="container-fluid program-home-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="program-title-infos-wrapper">
                        <div class="program-title">
                            <h2 class="primary-color"><?php _e('Program', 'congress'); ?></h2>
                        </div>
                        <div class="program-infos">
                            <div class="program-rooms">
                                <div class="programs-rooms-icon primary-background-color">
                                    <img src="<?php echo get_template_directory_uri()?>/inc/images/programs-rooms.png">
                                </div>
                                <div class="programs-rooms-title primary-color">
                                    <?php the_field('broj_sala'); ?>
                                </div>
                            </div>
                            <div class="program-days">
                                <div class="programs-days-icon primary-background-color">
                                    <?php echo $program_days = get_field('broj_dana'); ?>
                                </div>
                                <div class="program-days-text primary-color">
                                    <?php echo ($program_days == 1) ? __('day') : __('days'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-content text-color">
                        <?php the_field('tekst_programa'); ?>
                    </div>
                </div>
                <div class="col-md-4 program-banner-col">
                    <div class="program-baner-wrapper">
                        <img src="<?php the_field('slika_banera'); ?>">
                        <div class="program-cta">
                            <a class="cta-link" style="background: linear-gradient(89.89deg, <?php echo $primary_color; ?> -46.52%, <?php echo $gradient_color_1; ?> 54.98%);" 
                                href="<?php the_field('link_banera'); ?>"><?php _e('Get mobile app', 'congress'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PROGRAM SECTION END -->

<!-- SPONSORS SECTION -->
<div class="container-fluid sponsors-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="sponsors-head-title">
                    <h2 class="primary-color"><?php _e('Sponsors', 'congress'); ?></h2>
                </div>
                <div class="col-12 sponsors-wrapper">
                    <div class="row">
                            <?php 
                                $sponsors_class = get_field('klasa_sponzora');
                                if( $sponsors_class ) {
                                foreach( $sponsors_class as $sc ) { 
                                    $sponsors = $sc['sponzor'];
                                    $sponsors_num = count($sponsors);
                                    $col_class = ($sponsors_num > 4) ? 'col-2' : 'col-3';
                                ?>
                                <div class="col-12 single-sponsor-col">
                                    <div class="sp-class-name primary-color">
                                        <h4><?php echo $sc['ime_klase_spozora']; ?></h4>
                                    </div>
                                    <div class="primary-background-color sp-devider"></div>
                                    <div class="sponsors-images-wrapper">
                                        <div class="row">
                                            <?php 
                                                foreach($sponsors as $single_sponsor) { 
                                            ?>
                                                <div class="<?= $col_class; ?> sponsor-image-col">
                                                    <div class="sponsor-image-wrapper">
                                                        <a href="<?php echo $single_sponsor['sponsor_link']; ?>">
                                                            <img src="<?php echo $single_sponsor['sponsor_image']; ?>" >
                                                        </a>
                                                    </div>
                                                </div>    
                                            <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                } 
                            }
                        ?>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SPONSORS SECTION END -->

<!-- CTA BANERS START -->
<div class="container-fluid banners-home-fluid">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hm-banner-col">
                    <div class="cta-banner-wrap primary-background-color">
                        <div class="cta-banner-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet pretium magna.
                        </div>
                        <div class="cta-bannner-icon-wrapper section-background-color">
                            <img src="<?php echo get_template_directory_uri() . '/inc/images/text-book-icon.png' ?>">
                        </div>
                        <img class="banner-vector-top" src="<?php echo get_template_directory_uri() . '/inc/images/Vector 26.png'; ?>" >
                        <img class="banner-vector-bottom" src="<?php echo get_template_directory_uri() . '/inc/images/Vector 27.png'; ?>" >
                        <div class="cta-btn-link">
                            <a class="cta-banner-link" style="background: linear-gradient(89.89deg, <?php echo $primary_color; ?> -46.52%, <?php echo $gradient_color_1; ?> 54.98%);" href="#"><?php _e('Register', 'congress'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hm-banner-col">
                    <div class="cta-banner-wrap primary-background-color">
                        <div class="cta-banner-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet pretium magna.
                        </div>
                        <div class="cta-bannner-icon-wrapper section-background-color">
                            <img src="<?php echo get_template_directory_uri() . '/inc/images/sound-icon.png' ?>">
                        </div>
                        <img class="banner-vector-top" src="<?php echo get_template_directory_uri() . '/inc/images/Vector 26.png'; ?>" >
                        <img class="banner-vector-bottom" src="<?php echo get_template_directory_uri() . '/inc/images/Vector 27.png'; ?>" >
                        <div class="cta-btn-link">
                            <a class="cta-banner-link" style="background: linear-gradient(89.89deg, <?php echo $primary_color; ?> -46.52%, <?php echo $gradient_color_1; ?> 54.98%);" href="#"><?php _e('Oral presentation', 'congress'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hm-banner-col">
                    <div class="cta-banner-wrap primary-background-color">
                        <div class="cta-banner-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet pretium magna.
                        </div>
                        <div class="cta-bannner-icon-wrapper section-background-color">
                            <img src="<?php echo get_template_directory_uri() . '/inc/images/text-book-icon.png' ?>">
                        </div>
                        <img class="banner-vector-top" src="<?php echo get_template_directory_uri() . '/inc/images/Vector 26.png'; ?>" >
                        <img class="banner-vector-bottom" src="<?php echo get_template_directory_uri() . '/inc/images/Vector 27.png'; ?>" >
                        <div class="cta-btn-link">
                            <a class="cta-banner-link" style="background: linear-gradient(89.89deg, <?php echo $primary_color; ?> -46.52%, <?php echo $gradient_color_1; ?> 54.98%);" href="#"><?php _e('E-posters', 'congress'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CTA BANERS END -->


 
<?php
  //echo do_shortcode('[contact-form-7 id="81" title="Kontakt forma 1"]');
?>












<?php
get_footer();
?>


<script>
    var acfDate = "<?php echo $date->format('Y, m, d'); ?>";
    counDownFunc(acfDate);

    var swiper2 = new Swiper('.multi-slide-container', {
        loop: true,
        slidesPerView: 4,
        speed: 1400,
        spaceBetween: 40,
        navigation: {
            nextEl: '.swiper-button-next-unique',
            prevEl: '.swiper-button-prev-unique'
        },
        breakpoints: {
        // when window width is <= 991px
        991: {
            speed: 400,
        },   
        }
    });


</script>




