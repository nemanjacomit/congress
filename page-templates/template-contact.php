<?php
// Template Name: Kontakti
?>

<?php 
get_header();
?>


<div class="container-fluid">
    <div class="row">
    <div class="container pt-cont contact-main-cont">
        <div class="row">
            <div class="col-md-12 contact-info section-background-color">
                <div class="row contact-first-row">
                <?php
                $value_title_info = get_field( "naslov_leve_sekcije" );
                $value_title_supp = get_field( "naslov_sekcije_podrske" );
                //leva sekcija
                $location = get_field( "lokacija" );
                $location_icon = get_field( "ikonica_lokacije" );
                $zip = get_field( "zip_code" );
                $zip_icon = get_field( "zip_ikonica" );
                $city = get_field( "grad" );
                $city_icon = get_field( "grad_ikonica" );
                $mail = get_field( "linkovi" );
                $mail_icon = get_field( "mail_ikonica" );
                $mail_linkovi = "linkovi";
                //desna sekcija
                $phone_icon = get_field( "telefon_ikonica" );
                $phone_links = "telefon_brojevi";
                $mail_icon_2 = get_field( "mail_ikonica_2" );
                $mail_linkovi_2 = "mails";
                $phone_icon_2 = get_field( "telefon_ikonica_2" );
                $phone_links2 = "telefon_brojevi_2";
                function showItem($icon, $text){
                    if ($icon && $text) {
                    ?>
                    <div class="single-info">
                    <img class="" src="<?php echo $icon; ?>" alt="">
                    <!-- <div class="info-icon" style="background-image: url(<?php echo $icon; ?>);"> -->
                    <!-- </div> -->
                    <div class="info-name">
                        <p><?php echo $text ?></p>
                    </div>
                    </div>
                    <?php
                    }
                }
                function showMultiItems($icon, $field){
                    if ($icon && $field) {
                ?>      
                    <div class="multi-info">
                    <div class="img-div">
                    <img class="" src="<?php echo $icon; ?>" alt="">
                    </div>
                    <!-- <div class="info-icon" style="background-image: url(<?php echo $icon ?>);"></div> -->
                    <?php
                    if( have_rows($field) ){
                        // Loop through rows.
                    ?>
                    <div class="few-mail">
                    <?php
    
                        while( have_rows($field) ) {
                            the_row();
                            $date_mail_text = get_sub_field('link_text');
                            $date_mail_url = get_sub_field('link_url');
                    ?>
                        <div class="info-name">
                        <a href="date_mail_url" class="link-color"><p><?php echo $date_mail_text ?></p></a>
                        </div>
                    <?php
                        }
                    ?>
                    </div>
                    <?php
                    }
                    ?> 
                    </div>  
                <?php  
                }
                }
                ?>

                <div class="col-md-6">
                <p class="main-info-header primary-color"><?php echo $value_title_info; ?></p>
                <?php    
                showItem($location_icon, $location);
                showItem($zip_icon, $zip);
                showItem($city_icon, $city);
                showMultiItems($mail_icon, $mail_linkovi);
                ?> 
                </div>  
                <div class="offset-lg-1 col-12 col-md-6 col-lg-5">
                <div class="support-info-wrapper">
                <p class="support-info-header primary-color"><?php echo $value_title_supp; ?></p> 
                <?php
                showMultiItems($phone_icon, $phone_links);
                showMultiItems($mail_icon_2, $mail_linkovi_2);
                showMultiItems($phone_icon_2, $phone_links2);
                ?>
                </div>   
                </div>
                </div>
            </div>
        <div class="col-md-12 contact-form footer-background-color">
        
        <?php echo do_shortcode( '[contact-form-7 id="101" title="Nova forma"]' ); ?>
        </div>
        </div>
    </div>
    </div>
</div>




<?php 
get_footer();
?>

<style>
.contact-form input::placeholder,textarea::placeholder {
  color: <?= $text_color ?>;

}
.info-name a {
    color: <?php echo $link_color ?>;
  }
.submit-button {
background: linear-gradient(90deg, <?php echo $primary_color ?> -122.01%, <?php echo $gradient_color_1 ?> 50.05%);
}
</style>