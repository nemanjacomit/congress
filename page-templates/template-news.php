<?php
// Template Name: News
?>

<?php 
get_header();
?>

<style>
    .main-header h1::before{      
        background: <?php echo $gradient_color_1 ?>;
    }
    .pagination-wrapper .page-numbers {
        background-color: <?php echo $link_color ?>;
    }
    .pagination-wrapper .current {
        background-color: <?php echo $primary_color ?>;
    }
</style>

<div class="container-fluid">
    <div class="container home-hero-cont">
    <div class="container">
        <div class="row">
        <div class="col-md-12 main-header">
            <h1 class="text-color">News</h1>
        </div>
        <div class="col-md-12">
            <div class="row main-news-wrapper">
            <?php
                $currentPage = get_query_var('paged') ? get_query_var('paged') : 1;
                $args = array(
                    'category_name' => 'vesti',
                    'posts_per_page' => 4, // broj postova, ako napisemo -1 on ce ispisati sve postove koji odgovaraju uslovu ispod, tacnije event objavama     
                    'paged' => $currentPage,
                    'orderby' => 'date', //uzmi podatke od custom fielda nazmaceong u redu iznad
                    'order' => 'ASC',
                );
                $cats_posts = new WP_Query($args);
                while($cats_posts -> have_posts()) {
                    $cats_posts -> the_post();
                    get_template_part('template-parts/content','news');//pozivamo deo koda iz fdatog foldera
                }
            ?>
            </div>
            </div>
            <div class="pagination-wrapper">
            <?php 
                echo paginate_links(array(
                    'total' => $cats_posts->max_num_pages,
                    'prev_text'    => __('« Prev'),
                    'next_text'    => __('Next »')
                ));  
            ?>
            </div>
        </div>
    </div>
    </div>
</div>



<?php 
get_footer();
?>

<script>

</script>