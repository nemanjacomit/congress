<?php
// Template Name: Predavaci 
get_header();
?>


<div class="container-fluid">
    <div class="row">
        <div class="container home-hero-cont">
            <div class="row lecturer-row-wrapper">
            <div class="col-md-12 main-header primary-color">
                <h1 ><?php _e('Speakers', 'congress'); ?></h1>
            </div>
            <?php
                $args = array(
                    'post_type' => 'ucesnik',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'kategorije_ucesnika',
                            'field' => 'slug',
                            'terms' => 'speaker',
                        )
                    )
                );
                $lecturer_query = new WP_Query($args);
                $x = 0;
                while($lecturer_query -> have_posts() ) {
                    $lecturer_query -> the_post();
                ?>
                    <div class="col-lg-6 member-col">
                        <div class="single-speaker-wrapper">
                            <div class="lecturer-img">
                                <img src="<?php echo the_post_thumbnail_url('member-thumbnail'); ?>">
                            </div>
                            <div class="all-member-info">
                                <div class="lecturer-info text-color">
                                    <p class="lecturer-name"><?php the_title(); ?></p>
                                    <p class="lecturer-title"><?php the_field('titula_'); ?></p>
                                </div>
                                <div class="member-theme-time-wrapper text-color">
                                    <p><?php the_field('naziv_predavanja'); ?></p>
                                    <p><?php the_field('datum_predavanja'); ?> <?php _e(' from ', 'congress'); ?><?php the_field('pocetak_predavanja'); ?> -  <?php the_field('kraj_predavanja'); ?></p>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php  
                }
            ?>
            </div>
        </div>
    </div>
</div>


<?php 
get_footer();
?>
