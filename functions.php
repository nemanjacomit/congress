<?php
/**
 * congress functions and definitions
 *
 * @package congress
 */


// Congress API
require get_theme_file_path('/inc/api/congress-routes.php');

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */
function congress_setup() {

	load_theme_textdomain( 'congress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'congress' ),
		)
	);

}
add_action( 'after_setup_theme', 'congress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
// function congress_content_width() {
// 	$GLOBALS['content_width'] = apply_filters( 'congress_content_width', 640 );
// }
// add_action( 'after_setup_theme', 'congress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function congress_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'congress' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'congress' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'congress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function congress_scripts() {
	wp_enqueue_style( 'congress-style', get_stylesheet_uri(), array(), '1.0.0' );
	wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/inc/swiper/css/swiper.min.css', array(), '1.0.0');
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/inc/bootstrap-4.5.3/css/bootstrap.min.css', array(), '1.0.0' );
	wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/inc/css/custom.css', array(), '1.0.0' );
	wp_enqueue_style( 'stf-style', get_template_directory_uri() . '/inc/css/stf.css', array(), '1.0.0' );
	//wp_style_add_data( 'congress-style', 'rtl', 'replace' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/inc/swiper/js/swiper.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/inc/bootstrap-4.5.3/js/bootstrap.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/inc/js/custom.js', array(), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'congress_scripts' );

/**
 * Excerpt more
 */
function wporg_fcs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wporg_fcs_excerpt_more' );

/**
 * Excerpt limit
 */
function custom_excerpt($length) {
	return 25;
}
add_filter('excerpt_length', 'custom_excerpt', 999);


/**
 * Add image size
 */
add_image_size( 'custom-thumbnail', 590, 500, true );
add_image_size( 'member-thumbnail', 450, 470, true );



/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// if ( defined( 'JETPACK__VERSION' ) ) {
// 	require get_template_directory() . '/inc/jetpack.php';
// }

 
function cgs_customize_register( $wp_customize ) {

	$wp_customize->add_panel( 'SiteCustomizationOptions', array(
        'priority'       => 10,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => 'Opcije sajta',
        'description'    => '',
    ) );

	$wp_customize->add_section( 'BojeSajta', array(
        'title' => __( 'Boje sajta', 'congress' ),
        'priority' => 5,
        'panel' => 'SiteCustomizationOptions'
    ) );

    // Color field header
     $wp_customize->add_setting('color_L_header',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_header', array(
        'label' => __('Boja za header', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_header',
        'priority' => 4,
    ) ) );

    // Color 1 field
     $wp_customize->add_setting('color_L_1',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_1', array(
        'label' => __('Primarna boja', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_1',
        'priority' => 5,
    ) ) );

	 // Color 2 field
     $wp_customize->add_setting('color_L_2',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_2', array(
        'label' => __('Boja gradijenta', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_2',
        'priority' => 10,
    ) ) );

	 // Color 3 field
     $wp_customize->add_setting('color_L_3',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_3', array(
        'label' => __('Boja linka', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_3',
        'priority' => 15,
    ) ) );

      // Sections color
      $wp_customize->add_setting('color_L_section',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_section', array(
        'label' => __('Boja sekcije', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_section',
        'priority' => 17,
    ) ) );

    // Text color
    $wp_customize->add_setting('color_L_text',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_text', array(
        'label' => __('Boja teksta', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_text',
        'priority' => 20,
    ) ) );

    // Footer color
    $wp_customize->add_setting('color_L_footer',array(
        'default' => '#fff',
        'transport' => 'refresh',
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_control_L_footer', array(
        'label' => __('Boja za footer', 'congress'),
        'section' => 'BojeSajta',
        'settings' => 'color_L_footer',
        'priority' => 25,
    ) ) );

}
add_action('customize_register','cgs_customize_register');




// UCESNIK POST TYPE
function ucesnik_post_type(){
	$labels = array(
        'name'                  => _x( 'Učesnik kongresa', 'congress' ),
        'singular_name'         => _x( 'Učesnik kongresa', 'congress' ),
        'menu_name'             => _x( 'Učesnici kongresa', 'congress' ),
        'name_admin_bar'        => _x( 'Učesnici kongresa', 'congress' ),
        'add_new'               => __( 'Novi Učesnik', 'congress' ),
        'add_new_item'          => __( 'Novi Učesnik', 'congress' ),
        'new_item'              => __( 'Novi Učesnik', 'congress' ),
        'edit_item'             => __( 'Promeni Učesnika', 'congress' ),
        'view_item'             => __( 'Vidi Učesnika', 'congress' ),
    );
    $args = array(
        'labels' => $labels, 
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-admin-users',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false, 
        'publicly_queryable' => true,
        'supports' => array('title', 'thumbnail','editor' ),
		'show_in_rest' => true,
		'rest_base'    => 'ucesnik',
    );
	
    register_post_type('ucesnik', $args);
    }
add_action('init', 'ucesnik_post_type');
// TAXONOMY FOR UCESNIK
function kategorije_ucesnika() {
    register_taxonomy(
		'kategorije_ucesnika', 
		array('ucesnik'),  
		array(
			'label' => 'Kategorije ucesnika',
			'labels' => array(
				'add_new_item' => 'Dodaj novu kategoriju',
				'edit_item' => 'Izmeni kategoriju',
			),
			'show_in_menu' => true,
			'hierarchical' => true,
			'show_admin_column' => true
		)
    );
}

add_action('init', 'kategorije_ucesnika');

function vreme_ucesnika() {
    register_taxonomy(
		'vreme_predavanja', 
		array('ucesnik'),  
		array(
			'label' => 'Vreme i sala',
			'labels' => array(
				'add_new_item' => 'Dodaj vreme i salu',
				'edit_item' => 'Izmeni',
			),
			'show_in_menu' => true,
			'hierarchical' => true,
			'show_admin_column' => true
		)
    );
}
add_action('init', 'vreme_ucesnika');

// UCESNIK POST TYPE END





// PONUDE POST TYPE
function ponude_post_type(){
	$labels = array(
        'name'                  => _x( 'Ponuda', 'congress' ),
        'singular_name'         => _x( 'Ponuda', 'congress' ),
        'menu_name'             => _x( 'Ponude', 'congress' ),
        'name_admin_bar'        => _x( 'Ponuda', 'congress' ),
        'add_new'               => __( 'Nova Ponuda', 'congress' ),
        'add_new_item'          => __( 'Nova Ponuda', 'congress' ),
        'new_item'              => __( 'Nova Ponuda', 'congress' ),
        'edit_item'             => __( 'Promeni Ponudu', 'congress' ),
        'view_item'             => __( 'Vidi Ponudu', 'congress' ),
    );
    $args = array(
        'labels' => $labels, 
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-cart',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false, 
        'publicly_queryable' => true,
        'supports' => array('title', 'thumbnail','editor' ),
		'show_in_rest' => true,
		'rest_base'    => 'ponuda',
    );
	
    register_post_type('ponuda', $args);
    }
    add_action('init', 'ponude_post_type');
// TAXONOMY FOR PONUDE
function kategorije_ponuda() {
    register_taxonomy(
		'kategorije-ponuda', 
		array('ponuda'),  
		array(
			'label' => 'Kategorije Ponuda',
			'labels' => array(
				'add_new_item' => 'Dodaj novu kategoriju',
				'edit_item' => 'Izmeni kategoriju',
			),
			'show_in_menu' => true,
			'hierarchical' => true,
			'show_admin_column' => true
		)
    );
}
add_action('init', 'kategorije_ponuda');
// PONUDE POST TYPE END


// KOTIZACIJA POST TYPE
function kotizacija_post_type(){
	$labels = array(
        'name'                  => _x( 'Kotizacija', 'congress' ),
        'singular_name'         => _x( 'Kotizacija', 'congress' ),
        'menu_name'             => _x( 'Kotizacije', 'congress' ),
        'name_admin_bar'        => _x( 'Kotizacija', 'congress' ),
        'add_new'               => __( 'Nova Kotizacija', 'congress' ),
        'add_new_item'          => __( 'Nova Kotizacija', 'congress' ),
        'new_item'              => __( 'Nova Kotizacija', 'congress' ),
        'edit_item'             => __( 'Promeni Kotizaciju', 'congress' ),
        'view_item'             => __( 'Vidi Kotizaciju', 'congress' ),
    );
    $args = array(
        'labels' => $labels, 
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 10,
        'menu_icon' => 'dashicons-welcome-write-blog',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false, 
        'publicly_queryable' => true,
        'supports' => array('title', 'thumbnail','editor' ),
		'show_in_rest' => true,
		'rest_base'    => 'kotizacija',
    );
	
    register_post_type('kotizacija', $args);
    }
    add_action('init', 'kotizacija_post_type');
// KOTIZACIJA POST TYPE END