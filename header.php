<?php
/**
 * @package congress
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header">
		
		<div class="container-fluid header-fluid header-background-color">
			<div class="row">
				<div class="container">
					<div class="row logo-nav-row">
						<div class="col-md-3 logo-col">
							<div class="head-logo-wrapper">
								<?php the_custom_logo(); ?>
							</div>
						</div>
						<div class="col-md-9 menu-col">
							<div class="head-menu-wrapper text-color">
								<?php
									wp_nav_menu(
										array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										)
									);
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</header><!-- #masthead -->
<?php 
// GLOBAL COLORS
global $header_color;
global $primary_color;
global $gradient_color_1;
global $link_color;
global $text_color;
global $section_color ;
global $footer_color;
$header_color = get_theme_mod( 'color_L_header' );
$primary_color = get_theme_mod( 'color_L_1' );
$gradient_color_1 = get_theme_mod( 'color_L_2' );
$link_color = get_theme_mod( 'color_L_3' );
$text_color = get_theme_mod('color_L_text');
$section_color = get_theme_mod('color_L_section');
$footer_color = get_theme_mod('color_L_footer');
?>
<style>
	.text-color {color:<?php echo $text_color; ?> !important;}
	.background-text-color {background-color:<?php echo $text_color; ?> !important;}
	.header-color {color:<?php echo $header_color; ?> !important; }
	.header-background-color {background-color:<?php echo $header_color; ?> !important; }
	.primary-color {color:<?php echo $primary_color; ?> !important; }
	.primary-background-color {background-color:<?php echo $primary_color; ?> !important; }
	.gradient-color {color:<?php echo $gradient_color_1; ?>!important; }
	.background-gradient_color {background-color:<?php echo $gradient_color_1; ?>!important; }
	.link-color {color: <?php echo $link_color; ?>!important; }
	.link-background-color {background-color: <?php echo $link_color; ?>!important; }
	.section-color {color: <?php echo $section_color; ?>!important; }
	.section-background-color {background-color: <?php echo $section_color; ?>!important; }
	.footer-color {color: <?php echo $footer_color; ?>!important; }
	.footer-background-color {background-color: <?php echo $footer_color; ?>!important; }
</style>