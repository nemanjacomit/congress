<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package congress
 */

get_header();
?>

<div class="container-fluid">
    <div class="row">
    <div class="container home-hero-cont">
        <div class="row">
			<div class="col-md-12">
			<!-- <div class="crumb">
				<a href=" <?= home_url()?> "> Почетна </a> >>
				<a href=" <?= get_permalink( '169' ); ?>"><?= get_the_title( '169' ); ?> </a> >>
				<a  disabled><?= get_the_title( $post ) ?></a>
			</div> -->

			</div>
			<div class="col-md-12">
				<h1 class="single-post-title text-color"><?php the_title( ); ?></h1>
			</div>
			<div class="col-md-12"><img class="single-post-img" src="http://kongres.local/wp-content/uploads/2022/01/unsplash_wQV1706KWJ8.jpg" alt="" srcset="">
			</div>
			<div class="col-md-12 single-post-content text-color">
				<?php the_content( ); ?>
			</div>
		

		</div>
	</div>
	</div>
</div>





		


<?php

get_footer();
