<?php

// REGISTER ROUTES
add_action('rest_api_init', 'congressRegisterRoute');


function congressRegisterRoute() {

    // REGISTER ROUTE FOR POSTS-NEWS
    register_rest_route('congress/v1', 'news', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'congressNews',
        'permission_callback' => '__return_true'
        )
    );

    // REGISTER ROUTE FOR SPONSORS
    register_rest_route('congress/v1', 'sponsors', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'congressSponsors',
        'permission_callback' => '__return_true'
        )
    );

    // REGISTER ROUTE FOR SPEAKERS
    register_rest_route('congress/v1','speaker', array(
            'methods' => WP_REST_SERVER::READABLE,
            'callback' => 'congressSpeaker',
            'permission_callback' => '__return_true'
        )
    );

     // REGISTER ROUTE FOR OFFERS
    // register_rest_route('congress/v1', 'offers', array(
    //     'methods' => WP_REST_SERVER::READABLE,
    //     'callback' => 'congressOffers',
    //     'permission_callback' => '__return_true'
    //      )
    // );
}





// FUNCTION FOR POSTS
function congressNews() {
    $news = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => -1
    ));
    $newsResults = array();
    while($news->have_posts()){
        $news->the_post();
        array_push($newsResults, array(
            'id' => get_the_id(),
            'title' => get_the_title(),
            'content' => get_the_content(),
            'thumbnail' => has_post_thumbnail() ? get_the_post_thumbnail_url() : get_template_directory_uri(). '/inc/images/default-thumbnail.png',
            'date' => get_post_timestamp()
        ));
    }
    return $newsResults;
}
// FUNCTION FOR POSTS END





// FUNCTION FOR SPONSORS
function congressSponsors() {
    $allSponsors = array();
    $sponsors_class = get_field('klasa_sponzora', get_option('page_on_front'));
    if( $sponsors_class ) {
        foreach( $sponsors_class as $sc ) { 
            
        array_push($allSponsors, array(
            'sponsor_type' => $sc['ime_klase_spozora'],
            'sponsor_type_color' => $sc['boja_klase_sponzora'],
            'sponsors' => $sc['sponzor']
        ));
            
        }
    }
    return $allSponsors;
 }
// FUNCTION FOR SPONSORS END






// FUNCTION FOR SPEAKERS
function congressSpeaker() {
    $first_tabs = get_terms(array(
        'taxonomy' => 'vreme_predavanja',
        'hide_empty' => true,
        'parent'     => 0
    ));
    function nameStamp($i){
        $dtime = DateTime::createFromFormat("d.m.Y", $i->name);
        return $dtime->getTimestamp();
    }
    usort($first_tabs, function ($a, $b) {
        return nameStamp($a) - nameStamp($b);
    });

    $full_program = array();
    foreach ($first_tabs as $date_tab) {
        $child_terms = get_terms('vreme_predavanja', array(
            'hide_empty' => true,
            'parent' => $date_tab->term_id,
        ));
        $dtime = DateTime::createFromFormat("d.m.Y", $date_tab->name);
        $timestamp = $dtime->getTimestamp();
        $mbrs = array();
        foreach ($child_terms as $single_term) {
            $mbr = array();
            $members = get_posts(array(
                'post_type' => 'ucesnik',
                'posts_per_page' => -1,
                'meta_key' => 'pocetak_predavanja',
                'orderby' => 'meta_value_num',
                'order' => 'ASC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'vreme_predavanja',
                        'field' => 'term_id',
                        'terms' => $single_term->term_id,
                    ),
                    array(
                        'taxonomy' => 'kategorije_ucesnika',
                        'field' => 'slug',
                        'terms' => 'speaker'
                    )
                )
            ));
            foreach ($members as $member) {
                $mbr[] = array(
                    'member_name' => $member->post_title,
                    'member_theme' => get_field('naziv_predavanja', $member->ID),
                    'member_preofession' => get_field('titula_', $member->ID),
                    'start_time' => get_field('pocetak_predavanja', $member->ID),
                    'end_time' => get_field('kraj_predavanja', $member->ID),
                    'member_image' => get_the_post_thumbnail_url($member->ID, 'member-thumbnail')
                );
            }
            array_push($mbrs, array(
                'hall_name' => $single_term->name,
                'members' =>  $mbr
            ));
        }
        array_push($full_program, array(
            'date' => $timestamp,
            'members' =>  $mbrs
        ));
    }
    return $full_program;
}
// FUNCTION FOR SPEAKERS END




// FUNCTION FOR OFFERS
// function congressOffers() {

//     $terms = get_terms( array( 
//         'taxonomy' => 'kategorije-ponuda',
//         'parent'   => 0
//     ) );
//     if(!empty($terms)) { 
//     $newterms = array(); 
//     foreach($terms as $term) {
//         $order = get_field('broj_za_sortiranje', $term);             
//         $newterms[$order] = (object) array(
//             'name' => $term->name,
//             'slug' => $term->slug,
//             'term_id' => $term->term_id
//         );
//     }
//     krsort( $newterms, SORT_NUMERIC );
//     $fullOffer = array();

//     foreach($newterms as $nTerm) {
//         $term_slug = $nTerm->slug;
//         $term_name = $nTerm->name;
//         $termArgs = array(
//             'post_type'     => 'ponuda',
//             'tax_query' => array(
//                 array(
//                 'taxonomy' => 'kategorije-ponuda',
//                 'field'    => 'slug',
//                 'terms'    => $term_slug,
//                 ),
//             ),
//             'posts_per_page' => -1
//         );
        
//         $offers = new WP_Query($termArgs);
//         $offersResults = array();
//         while($offers->have_posts()){
//             $offers->the_post();

//             $amenities = array();
//             $offer_options = get_field('opcije_ponude');
//             foreach($offer_options as $ofo) {
//                 array_push($amenities, array(
//                     'title' => $ofo['opcija'],
//                     'isIncluded' => $ofo['ne_ulazi_u_ponudu'][0] ? false : true
//                 ));
//             }
//             array_push($offersResults, array(
//                 'id' => get_the_id(),
//                 'title' => get_the_title(),
//                 'price' => get_field('cena_ponude'),
//                 'content' => get_the_content(),
//                 'date' => get_post_timestamp(),
//                 'amenities' => $amenities
//             ));
//         }
    
//         array_push($fullOffer, array(
//             'room_type' => $term_name,
//             'offers' => $offersResults
//         ));
//     }
//     return $fullOffer;
//     }
// }
// FUNCTION FOR OFFERS END