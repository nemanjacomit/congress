<?php
/**
 * congress Theme Customizer
 *
 * @package congress
 */







function congress_customize_fields() { 
	/*
	* Switch default core markup for search form, comment form, and comments
	* to output valid HTML5.
	*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	// add_theme_support(
	// 	'custom-background',
	// 	apply_filters(
	// 		'congress_custom_background_args',
	// 		array(
	// 			'default-color' => 'ffffff',
	// 			'default-image' => '',
	// 		)
	// 	)
	// );


	/**
	 * Add support for core custom logo.
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => '',
			'width'       => '',
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'congress_customize_fields' );











/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
// function congress_customize_register( $wp_customize ) {
// 	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
// 	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
// 	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

// 	if ( isset( $wp_customize->selective_refresh ) ) {
// 		$wp_customize->selective_refresh->add_partial(
// 			'blogname',
// 			array(
// 				'selector'        => '.site-title a',
// 				'render_callback' => 'congress_customize_partial_blogname',
// 			)
// 		);
// 		$wp_customize->selective_refresh->add_partial(
// 			'blogdescription',
// 			array(
// 				'selector'        => '.site-description',
// 				'render_callback' => 'congress_customize_partial_blogdescription',
// 			)
// 		);
// 	}
// }
// add_action( 'customize_register', 'congress_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
// function congress_customize_partial_blogname() {
// 	bloginfo( 'name' );
// }

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
// function congress_customize_partial_blogdescription() {
// 	bloginfo( 'description' );
// }








	// Add theme support for selective refresh for widgets.
//	add_theme_support( 'customize-selective-refresh-widgets' );



/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
// function congress_customize_preview_js() {
// 	wp_enqueue_script( 'congress-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
// }
// add_action( 'customize_preview_init', 'congress_customize_preview_js' );
