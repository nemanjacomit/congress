// Countdown
// Set the date we're counting down to
var countDownDate = new Date(counter_race.start_date + " " + counter_race.start_time).getTime();

// export { countDownDate };

// Update the count down every 1 second
var x = setInterval(function () {
    
    // Get today's date and time
    var now = new Date().getTime();
    
    // Find the distance between now and the count down date
    var distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    function getZero(time) {
        if (time < 10) {
            time = '0' + time;
        }
        return time;
    }
    
    // Display the result in the element with id="time-counter"
    document.getElementById("time-counter").innerHTML = `
    <div><span>${getZero(days)}</span> <span>Days</span></div>
    <div><span>${getZero(hours)}</span> <span>Hours</span></div>
    <div><span>${getZero(minutes)}</span> <span>Minutes</span></div>
    <div><span>${getZero(seconds)}</span> <span>Seconds</span></div>
    `;
    
    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("time-counter").innerHTML = "EXPIRED";
    }
}, 1000);