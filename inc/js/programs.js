"use strict";

const mainContainer = document.querySelector(".main-program-container");
const datesContainer = document.querySelector(".date-boxes-wrapper");
const hallContainer = document.querySelector(".hall-boxes-wrapper");

const itemContainers = document.querySelector(".program-items-wrapper");

/*Daj boze da radi START*/

const url = "https://kongres.comit.in.rs/wp-json/congress/v1/program";
let pdfLink = "";
fetch(url)
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    // objekat = response;
    // console.log(data);
    pdfLink = data.downloadPdf;
    // console.log(pdfLink);
    const dates = data.speakers;
    // console.log(typeof(dates));
    let datesArray = [];
    let date_set = 0;
    let hall_set = 0;
    dates.map(function (el, i) {
      // Citanje datuma
      date_set = i;
      const r_value = el.date;
      const date = new Date(r_value);
      const full_date = `${date.getMonth()}.${date.getDate()}`;
      // console.log(r_value);
      // console.log(date.getHours());
      const date_html = `
    <div class="date-box-wrapper" data-date = "${date_set}">
        <p class="date-box-day">Petak</p>
        <p class="date-box-date">${full_date}</p>
    </div>
    `;
      datesContainer.insertAdjacentHTML("beforeend", date_html);
      //Citanje hala
      const halls = el.conference_rooms;
      halls.map(function (el, i) {
        hall_set = i;
        const hall_name = el.title;
        const hall_html = `
        <div class="hall-box-wrapper item-hidden" data-parent="${date_set}" data-hall = "${hall_set}">
            <p class="hall-box-name">${hall_name}</p>
        </div>
        `;
        hallContainer.insertAdjacentHTML("beforeend", hall_html);
        //CItanje predavaca

        const lecturer = el.lecturers;
        lecturer.map(function (el, i) {
          const name = el.name;
          const subject = el.subject;
          const workplace = el.workplace;
          const start_time = el.start_time;
          const end_time = el.end_time;
          const image = el.image;

          const lecturer_html = `
            <div class="item-margin-wrapper item-hidden" data-item = "${date_set
              .toString()
              .concat(hall_set.toString())}">
            <div class="signle-item-wrapper-2 " >
            <div class="image-wrapper">
            <img src="${image}" alt="" srcset="">
            </div>
            <div class="item-info-wrapper">
            <div class="container info-text-wrapper">
            <div class="row info-text-wrapper-row">
            <div class="col-md-10 info-text-left">
            <p class="item-topic">${subject}</p>
            <div class="lecturer-info-wrapper">
            <p class="item-lecturer-name">${name} &nbsp; </p>
            <p class="item-lecturer-info">${workplace}</p>
            </div>
            </div>
            <div class="col-md-2 info-text-right">
            <p>${start_time} - ${end_time}</p>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="signle-item-margin">
            <div class="image-wrapper">
            </div>
            <div class="item-info-wrapper">
            <div class="container info-text-wrapper">
            <div class="row info-text-wrapper-row">
            <div class="col-md-10 info-text-left-margin">
            
            </div>
            <div class="col-md-2 info-text-right-margin">
            
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            `;
          itemContainers.insertAdjacentHTML("beforeend", lecturer_html);
        });
      });
      // console.log(el.conference_rooms);
    });

    mainContainer.style.height = "100%";

    const dateContainers = document.querySelectorAll(".date-box-wrapper");
    const hallContainers = document.querySelectorAll(".hall-box-wrapper");
    const items = document.querySelectorAll("[data-item]");

    const firstActiveDate = document.querySelector("[data-date='0']");
    const firstActiveHall = document.querySelector("[data-hall='0']");
    const firstVisibleHall = document.querySelector("[data-parent='0']");
    let firstSet = "0",
      secoundSet = "0";

    const shawAll = function () {
      //DECLARE FUNCTION
      const activateContent = function () {
        // Activate content area
        items.forEach((item) => {
          if (item.dataset.item == firstSet.concat(secoundSet)) {
            item.classList.remove("item-hidden");
          } else {
            item.classList.add("item-hidden");
          }
        });
      };

      //When click hall
      const showItemsHall = function () {
        activateContent();

        hallContainers.forEach((item) => {
          // console.log(firstSet, item.dataset.parent);
          if (item.dataset.parent == firstSet) {
            item.classList.remove("item-hidden");
          } else {
            item.classList.add("item-hidden");
          }
        });
      };

      //When click date
      const showItems = function () {
        activateContent();
        let execute = true;
        hallContainers.forEach((item) => {
          // console.log(firstSet, item.dataset.parent);
          if (item.dataset.parent == firstSet) {
            item.classList.remove("item-hidden");
            if (execute) {
              item.classList.add("hall-box--active");
              item.click();
            }
            execute = false;
          } else {
            item.classList.add("item-hidden");
          }
        });
      };
      showItems();

      const showActive = function () {
        firstActiveDate.classList.add("date-box--active");
        firstActiveHall.classList.add("hall-box--active");
        firstVisibleHall.classList.remove("item-hidden");
      };
      showActive();

      //DEFINED CLICKS
      datesContainer.addEventListener("click", function (e) {
        e.preventDefault();
        const clicked = e.target.closest(".date-box-wrapper");
        if (!clicked) return;
        // Remove active classes
        dateContainers.forEach((t) => t.classList.remove("date-box--active"));
        // Activate tab
        clicked.classList.add("date-box--active");
        firstSet = clicked.dataset.date;
        showItems();
        console.log("radi");
      });
      hallContainer.addEventListener("click", function (e) {
        const clicked = e.target.closest(".hall-box-wrapper");

        if (!clicked) return;
        hallContainers.forEach((t) => t.classList.remove("hall-box--active"));
        clicked.classList.add("hall-box--active");
        secoundSet = clicked.dataset.hall;
        showItemsHall();
      });
    };
    shawAll();
  })
  .catch(function (error) {
    console.log("Nesto ne valja");
  });
