<?php
/*

**Template part for single news**

*/ 

?>


<div class="news-wrapper">
    <div class="news-info-wrapper">
        <?php 
        $url_img = the_post_thumbnail('thumbnail');
        if($url_img){
        $url = $url_img;
        }else{
            $url = 'http://kongres.local/wp-content/uploads/2022/01/unsplash_wQV1706KWJ8.jpg';
        }
        ?>
        <img src="<?php $url; ?>">
        <div class="info">
            <div class="info-date">
                <div class="icon" style="background-image: url(<?php echo get_template_directory_uri().'/inc/images/watch_icon.png' ?>);" ></div>
                <p><?php echo get_the_date('d.m.Y'); ?></p>
            </div>
            <div class="info-text">
            <p class="title text-color"> <?php echo the_title( );  ?> </p>
            <hr class="text-background-color">
            <p class="content text-color"> <?php echo wp_trim_words( get_the_content(  ), 20 )  ?> </p>
            </div>
        </div>
    </div>
    <a href="<?php echo get_permalink( ); ?>"><button class="news-read-more link-background-color" >Read more</button></a>
</div>