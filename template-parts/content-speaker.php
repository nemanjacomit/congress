<?php
/*

**Template part for single speaker on speakers page**

*/ 

?>

<div class="lecturer-item">
    <div class="lecturer-img" src="">
    <img src="<?php echo the_post_thumbnail_url() ?>" alt="" srcset="">
    </div>

    <div class="lecturer-info">
        <p class="lecturer-name"><?= the_title( ) ?></p>
        <p class="lecturer-title">Professor</p>
    </div>
    
</div>