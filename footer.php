<?php
/**
 * @package congress
 */
?>
	<footer id="colophon" class="site-footer footer-background-color">
		<div class="container-fluid footer-fluid">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-md-7 footer-logo-txt-col">
							<div class="footer-logo-wrapper">
								<img src="<?php echo get_template_directory_uri() . '/inc/images/footer-logo.png' ?>">
							</div>
							<div class="footer-txt-wrapper">
								<p>The first world conference fighting a COVID-19 pandemic health challanges</p>
							</div>
							<div class="footer-vertical-devider"></div>
						</div>
						<div class="col-md-5">
							<div class="footer-info-wrapper">
								<h4 class="text-color"><?php _e('Serbian Neurosurgical society','congress') ?></h4>
								<div class="footer-info-row">
									<img src="<?php echo get_template_directory_uri() . '/inc/images/footer-tel-icon.png'; ?>">
									<div class="footer-info-links">
										<a class="text-color" href="tel:+381113348158">+381 11 334 81 58</a>
									</div>
								</div>
								<div class="footer-info-row">
									<img src="<?php echo get_template_directory_uri() . '/inc/images/footer-mail-icon.png'; ?>">
									<div class="footer-info-links">
										<a class="text-color" href="tel:+381113348158">+381 11 334 81 58</a>
									</div>
								</div>
								<div class="footer-info-row">
									<img src="<?php echo get_template_directory_uri() . '/inc/images/footer-fax-image.png'; ?>">
									<div class="footer-info-links">
										<a class="text-color" href="tel:+381113348158">+381 11 334 81 58</a>
										<a class="text-color" href="tel:+381113348158">+381 11 334 81 58</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 footer-horisontal-devider"></div>
						<div class="col-12 footer-bootom-col">
							<div class="footer-bootoms-wrapper">
								<div class="terms-conditions-wrapper">
									<a class="text-color" href="#"><?php _e('Terms and conditions', 'congress'); ?></a>
								</div>
								<div class="footer-apps-wrapper">
									<a href="#"><img src="<?php echo get_template_directory_uri() . '/inc/images/andr-app-icon.png'; ?>" alt=""></a>
									<a href="#"><img src="<?php echo get_template_directory_uri() . '/inc/images/ios-app-icon.png'; ?>" alt=""></a>
									<a href="#"><img src="<?php echo get_template_directory_uri() . '/inc/images/qrcode1.png'; ?>" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>



	
</body>
</html>
